class Nodo
{
    constructor(tipo,valor,nivel,padre,sonright,sonleft) {
        this.tipo=tipo;
        this.valor=valor;
        this.nivel=nivel;
        this.padre=padre;
        this.hijoDerecho=sonright;
        this.hijoIzquierdo=sonleft;

        if(sonright=="d")
            this.HijoDerecho=this.crearHijo("D",0,nivel+1,1,0,0,this.nivel);
        if(sonleft=="-")
            this.HijoIzquierdo = this.crearHijo("-", 0, nivel + 1, 1,"*","A", this.nivel);
        if(sonright=="*")
                this.HijoDerecho=this.crearHijo("*",0,nivel+1,1,"B","C",this.nivel);
        if(sonleft=="a")
            this.HijoIzquierdo = this.crearHijo("A", 0, nivel + 1, 1,0,0, this.nivel);
        if(sonright=="b")
            this.HijoDerecho=this.crearHijo("B",0,nivel+1,1,0,0,this.nivel);
        if(sonleft=="c")
            this.HijoIzquierdo = this.crearHijo("C", 0, nivel + 1, 1,0,0, this.nivel);
    }
    crearHijo(tipo,valor,nivel,padre,sonright,sonleft){
        var CrearHijo=new Nodo(tipo,valor,nivel,padre,sonright,sonleft);
        return CrearHijo;
    }
}