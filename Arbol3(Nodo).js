class Nodo{
constructor(nombre,nivel,padre,sonleft,sonright) {
    var aux=0;
    this.nombre=nombre;
    this.nivel=nivel;
    this.padre=padre;
    this.HL=sonleft;
    this.HR=sonright;
    if(sonleft==16)
        this.HL=this.crearHijo(16,nivel+1,padre+1,8,8);
    if(sonright==20)
        this.HR=this.crearHijo(20,nivel+1,padre+1,10,64)
    if(sonleft==8)
        this.HL=this.crearHijo(8,nivel+1,padre+1,4,3);
    if(sonright==8)
        this.HR=this.crearHijo(8,nivel+1,padre+1,3,5);
    if(sonleft==4)
        this.HL=this.crearHijo("e",nivel+1,padre+1,0,0);
    if(sonright==3)
        this.HR=this.crearHijo(4,nivel+1,padre+1,10,10);
    if(sonleft==3)
        this.HL = this.crearHijo("a", nivel + 1, padre + 1, 0, 0);
    if(sonright==5)
        this.HR=this.crearHijo(4,nivel+1,padre+1,2,2);
    if(sonleft==10)
        this.HL = this.crearHijo("n", nivel + 1, padre + 1, 0, 0);
    if(sonright==10)
        this.HR=this.crearHijo(2,nivel+1,padre+1,0,0);
    if(sonleft==2)
        this.HL = this.crearHijo("t", nivel + 1, padre + 1, 0, 0);
    if(sonright==2)
        this.HR=this.crearHijo("m",nivel+1,padre+1,0,0);
}
crearHijo(nombre,nivel,padre,sonleft,sonright){
    var crearHijo=new Nodo(nombre,nivel,padre,sonleft,sonright);
    return crearHijo;
}
}