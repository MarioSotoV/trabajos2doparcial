class son{
    constructor(tipo,valor,nivel,father,sonright,sonleft) {
        this.tipo=tipo;
        this.valor=valor;
        this.nivel=nivel;
        this.padre=father;
        this.hijoDerecho=sonright;
        this.hijoIzquierdo=sonleft;

        if(sonright=="d")
            this.HijoDerecho=this.construirSon("d",0,nivel+1,1,0,0,this.nivel);
        if(sonleft=="-")
            this.HijoIzquierdo = this.construirSon("-", 0, nivel + 1, 1,"*","a", this.nivel);

    }
    construirSon(tipo,valor,nivel,father,sonright,sonleft){
        var construirSon=new son(tipo,valor,nivel,father,sonright,sonleft);
        return construirSon;
    }
}